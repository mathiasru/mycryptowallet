package exceptions;
//Abschnitt F
public class InvalidAmountException extends Exception {
    public InvalidAmountException() {
        super("Invalid Amount < 0");
    }
}
