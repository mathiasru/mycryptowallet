package exceptions;
//Abschnitt J
public class SaveDataException extends Exception {
    public SaveDataException(String message) {
        super(message);
    }
}
