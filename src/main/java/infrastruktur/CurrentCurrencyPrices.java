package infrastruktur;

import domain.CryptoCurrency;
import domain.CurrentPriceForCurrency;
import exceptions.GetCurrentPriceException;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

/**
 * Die Klasse implementiert einen HttpRequest, welcher mittels Rest-API den aktuellen Kurs jeder jeweiligen
 * Kryptowährung ausliest und richtig geparst wird.
 *
 * @author mathiasrudig
 * @version 1.0
 */
//Abschnitt I
public class CurrentCurrencyPrices implements CurrentPriceForCurrency {
    /**
     * Die Methode implementiert einen HttpRequest, welcher mittels Rest-API den aktuellen Kurs jeder jeweiligen
     * Kryptowährung ausliest und richtig geparst wird um am Ende als BigDecimal zurückgeliefert zu werden. Zudem
     * werden mehrere Exception direkt innerhalb der Methode gehandelt (IOException, InterruptedException,
     * NumberFormatException) und im Falle auch ausgegeben. Die Exception(GetCurrentPriceException) muss Außen
     * behandelt werden.
     *
     * @param cryptoCurrency übergebe Kryptowährung für den Request vom Typ CryptoCurrency
     * @return gibt den aktuellen Kryptokurs vom Typ BigDecimal zurück.
     * @throws GetCurrentPriceException
     */
    //Abschnitt I
    @Override
    public BigDecimal getCurrentPrice(CryptoCurrency cryptoCurrency) throws GetCurrentPriceException {
        //HttpClient erzeugen für Get, Put Requests und werden mit .newHttpClient() (statische Methode) initialisiert
        HttpClient client = HttpClient.newHttpClient();
        //Generieren eines HttpRequest, der auf eine implementierte URI angewendet wird und akzeptiert einen mitgegebenen Header
        HttpRequest request = HttpRequest.newBuilder(
                URI.create("https://api.coingecko.com/api/v3/simple/price?ids="
                        + cryptoCurrency.currencyName
                        + "&vs_currencies=eur"))
                .header("accept", "application/json")
                .build();
        try {
            //Auslesen per Anfrage
            HttpResponse<String> result = client.send(request, HttpResponse.BodyHandlers.ofString());
            //Parsen unseres Request-Ergebnisses
            //Splitten der Json-Datei nach jedem ":" und speichern in ein String-Array
            String[] spilt = result.body().split(":");
            //aktuellen Wert aus dem String-Array holen
            String result2 = spilt[2].substring(0, spilt[2].length() - 2); //-2 um die Klammern wegzulassen
            return new BigDecimal(result2);
        } catch (IOException ioException) {
            ioException.printStackTrace();
            throw new GetCurrentPriceException("IOException: " + ioException.getMessage());
        } catch (InterruptedException interruptedException) {
            interruptedException.printStackTrace();
            throw new GetCurrentPriceException("InterruptedException: " + interruptedException.getMessage());
        } catch (NumberFormatException numberFormatException) {
            throw new GetCurrentPriceException("Conversion of Value not possible: " + numberFormatException.getMessage());
        }
    }
}

