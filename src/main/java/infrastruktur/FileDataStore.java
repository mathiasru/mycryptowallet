package infrastruktur;

import exceptions.RetrieveDataException;
import exceptions.SaveDataException;
import domain.BankAccount;
import domain.DataStore;
import domain.WalletList;
import java.io.*;

/**
 * Die Klasse ist für das Speichern und Auslesen in/aus Binärdateien von bestehenden Objekten zuständig und
 * implementiert die Methoden vom Interface DataStore.Wichtig dabei ist, dass die betroffenen Klassen das
 * Marker-Interface Serializable implmentieren um das Speichern/Auslesen realisieren zu können.
 *
 * @author mathiasrudig
 * @version 1.0
 */
//Abschnitt J
public class FileDataStore implements DataStore {
    /**
     * Die Methode implementiert das Speichern von übergebenen Objekten vom Typ BankAccount und speichert diese
     * als Binärdatei ab.
     * @param bankAccount übergebener BankAccount vom Typ BankAccount
     * @throws SaveDataException
     */
    //Abschnitt J
    @Override
    public void saveBankAccount(BankAccount bankAccount) throws SaveDataException {
        if (bankAccount != null) {
            //Die Klasse ObjectOutputStream ermöglicht das Speichern von Objekten in eineBinärdatei über die Hilfsklasse FileOutput.
            ObjectOutputStream objectOutputStream = null;
            try {
                objectOutputStream = new ObjectOutputStream(new FileOutputStream("account.bin"));
                //Schreiben des übergebenen Objektes in Datei
                objectOutputStream.writeObject(bankAccount);
                objectOutputStream.close();
            } catch (IOException ioException) {
                ioException.printStackTrace();
                throw new SaveDataException("Error saving BankAccount to file: " + ioException.getMessage());
            }
        }
    }

    /**
     * Die Methode implementiert das Speichern von übergebenen Objekten vom Typ Walletlist und speichert diese
     * als Binärdatei ab.
     * @param walletList übergebene Walletlist vom Typ Walletlist
     * @throws SaveDataException
     */
    //Abschnitt K
    @Override
    public void saveWalletlist(WalletList walletList) throws SaveDataException {
        if (walletList != null) {
            ObjectOutputStream objectOutputStream = null;
            try {
                objectOutputStream = new ObjectOutputStream(new FileOutputStream("walletlist.bin"));
                objectOutputStream.writeObject(walletList);
                objectOutputStream.close();
            } catch (IOException ioException) {
                ioException.printStackTrace();
                throw new SaveDataException("Error saving Walletlist to file:" + ioException.getMessage());
            }
        }
    }

    /**
     * Die Methode implementiert das Auslesen von Objekten vom Typ BankAccount aus einer Binärdatei und gibt diese
     * dann zurück.
     * @return gibt einen BankAccount von Typ BankAccount zurück
     * @throws RetrieveDataException
     */
    //Abschnitt J
    @Override
    public BankAccount retrieveBankAccount() throws RetrieveDataException {
        ObjectInputStream objectInputStream = null;
        try {
            objectInputStream = new ObjectInputStream(new FileInputStream("account.bin"));
            //Auslesen der Datei und zuweisung an Objekt (hier müsste ggf. auf den Typ gefrüft werden mittels instanceOf)
            BankAccount bankAccount = (BankAccount)objectInputStream.readObject();
            objectInputStream.close();
            return bankAccount;
        } catch (IOException | ClassNotFoundException exception) {
            exception.printStackTrace();
            throw new RetrieveDataException("Error on retrieving BankAccount Data from file: " + exception.getMessage());
        }
    }

    /**
     * Die Methode implementiert das Auslesen von Objekten vom Typ WalletList aus einer Binärdatei und gibt diese
     * dann zurück.
     * @return gibt einen WalletList von Typ WalletList zurück
     * @throws RetrieveDataException
     */
    //Abschnitt K
    @Override
    public WalletList retrieveWalletList() throws RetrieveDataException {
        ObjectInputStream objectInputStream = null;
        try {
            objectInputStream = new ObjectInputStream(new FileInputStream("walletlist.bin"));
            //Auslesen der Datei und zuweisung an Objekt (hier müsste ggf. auf den Typ gefrüft werden mittels instanceOf)
            WalletList walletList = (WalletList)objectInputStream.readObject();
            objectInputStream.close();
            return walletList;
        } catch (IOException | ClassNotFoundException exception) {
            exception.printStackTrace();
            throw new RetrieveDataException("Error on retrieving BankAccount Data from file: " + exception.getMessage());
        }
    }
}
