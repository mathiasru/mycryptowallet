package domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.UUID;

/**
 * Die Klasse impleementiert eine Buchungszeile, welche nach dem Erstellen nicht mehr veränderbar ist(final). Das Ganze
 * wird über den Konstruktor initialisiert. Zudem werden alle Datenfelder per sondierender-Methode bereitgestellt.
 *
 * @author mathiasrudig
 * @version 1.0
 */
//Abschnitt D
public class Transaction implements Serializable {
    private final UUID id;
    private final CryptoCurrency cryptoCurrency;
    private final BigDecimal amount;
    private final BigDecimal priceAtTransactionDate;
    private final LocalDate date;
    private final BigDecimal total;

    /**
     * Der Konstruktor setzt eine Buchungszeile aus übergebenen Werten wie CryptoCurrency, amount und
     * priceAtTransactionDate und aktuellen Werten sowie berechneten Werten zusammen.
     *
     * @param cryptoCurrency         übergebene Kryptowährung vom Typ CryptoCurrency
     * @param amount                 übergebener Betrag vom Typ BigDecimal
     * @param priceAtTransactionDate übergebener aktueller Kurs vom Typ BigDecimal
     */
    //Abschnitt D
    public Transaction(CryptoCurrency cryptoCurrency, BigDecimal amount, BigDecimal priceAtTransactionDate) {
        this.id = UUID.randomUUID();
        this.cryptoCurrency = cryptoCurrency;
        this.amount = amount;
        this.priceAtTransactionDate = priceAtTransactionDate;
        this.date = LocalDate.now();
        this.total = this.amount.multiply(this.priceAtTransactionDate.setScale(6, RoundingMode.HALF_UP));
    }

    public UUID getId() {
        return id;
    }

    public CryptoCurrency getCryptoCurrency() {
        return cryptoCurrency;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public BigDecimal getPriceAtTransactionDate() {
        return priceAtTransactionDate;
    }

    public LocalDate getDate() {
        return date;
    }

    public BigDecimal getTotal() {
        return total;
    }

    /**
     * Die Methode gibt alle Datenfelder in Form eines Strings zurück.
     *
     * @return Datenfelder vom Typ String
     */
    //Abschnitt D
    @Override
    public String toString() {
        return "Transaction{" +
                "id=" + id +
                ", cryptoCurrency=" + cryptoCurrency +
                ", amount=" + amount +
                ", priceAtTransactionDate=" + priceAtTransactionDate +
                ", date=" + date +
                ", total=" + total +
                '}';
    }
}
