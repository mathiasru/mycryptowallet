package ui;

import at.itkolleg.sample.WalletApp;
import domain.CryptoCurrency;
import domain.Wallet;
import exceptions.InsufficientBalanceException;
import exceptions.InvalidFeeException;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.math.BigDecimal;
import java.util.Optional;

/**
 * Die Klasse implementiert die Logik und das Verhalten der Grafischen Oberfläche der Hauptseite und erbt von der Klasse
 * BaseControllerState.
 *
 * @author mathiasrudig
 * @version 1.0
 */
public class MainController extends BaseControllerState {
    //FXML-IDs  //Abschnitt S
    @FXML
    private Button btnClose;

    @FXML
    private ComboBox cmbWalletCurrency;

    @FXML
    private Label lblBankaccountBalance;

    @FXML
    private TableView<Wallet> tableView;

    /**
     * Die Methode wird zum Initialisieren der Grafischen Oberfläche verwendet.
     */
    public void initialize() {
        //Abschnitt Refactoring
        this.populateTable();
        this.refreshAllGuiValues();

        //Abschnitt Z
        this.btnClose.setOnAction((ActionEvent event) -> { //könnte auch in eine seperate Methode implementiert werden
            Platform.exit();
        });
    }

    /**
     * Die Methode implementiert das Aktualisieren der Datenfelder, welche angezeigt werden.
     */
    //Abschnitt Refactoring
    private void refreshAllGuiValues() {
        this.cmbWalletCurrency.getItems().addAll(CryptoCurrency.getCodes());
        this.lblBankaccountBalance.textProperty().setValue(getBankAccount().getBalance().toString());
        //Einträge definieren aus der Walletlist
        tableView.getItems().setAll(getWalletList().getWallesAsObservabletList());
    }

    /**
     * DIe Methode implementiert das Erzeugen einer Tabelle mit definierten Spalten und fügt Datensätze aus der
     * Wallet hinzu.
     */
    //Abschnitt Refactoring
    private void populateTable() {
        //Spalten der Tabelle definieren über Getter-Methoden
        TableColumn<Wallet, String> symbol = new TableColumn<>("SYMBOL");
        symbol.setCellValueFactory(new PropertyValueFactory<>("cryptoCurrency"));

        TableColumn<Wallet, String> currencyName = new TableColumn<>("CURRENCY NAME");
        currencyName.setCellValueFactory(new PropertyValueFactory<>("currencyName"));

        TableColumn<Wallet, String> name = new TableColumn<>("WALLET NAME");
        name.setCellValueFactory(new PropertyValueFactory<>("name"));

        TableColumn<Wallet, String> amount = new TableColumn<>("AMOUNT");
        amount.setCellValueFactory(new PropertyValueFactory<>("amount"));

        //Spalten hinzufügen
        tableView.getColumns().clear(); //zu erst alles löschen
        tableView.getColumns().add(name);
        tableView.getColumns().add(symbol);
        tableView.getColumns().add(currencyName);
        tableView.getColumns().add(amount);
    }

    /**
     * Die Methode implementiert das Einzahlen über ein Pop-up-Dialogfenster, welches über die bereitgestellten Methoden
     * mit Textinhalten befüllt wird.
     */
    //Abschnitt R
    public void deposit() {
        TextInputDialog dialog = new TextInputDialog("Insert amount to deposit");
        dialog.setTitle("Deposit to bankaccount");
        dialog.setHeaderText("How much money do you want to deposit?");
        dialog.setContentText("Amount: ");
        Optional<String> result = dialog.showAndWait(); //liefert ein Optinal zurück
        if (result.isPresent()) { //wenn etwas zurück geliefert wurde
            try {
                BigDecimal amount = new BigDecimal(result.get());
                this.getBankAccount().deposit(amount);
                this.lblBankaccountBalance.textProperty().setValue(this.getBankAccount().getBalance().toString());
                System.out.println("DEPOSIT");
            } catch (NumberFormatException numberFormatException) { //unchecked Exception
                WalletApp.showErrorDialog("Please insert a number!");
            }
        }
    }

    /**
     * Die Methode implementiert das Abheben über ein Pop-up-Dialogfenster, welches über die bereitgestellten Methoden
     * mit Textinhalten befüllt wird.
     */
    //Abschnitt R
    public void withdraw() {
        TextInputDialog dialog = new TextInputDialog("Insert amount to withdraw");
        dialog.setTitle("Withdraw from bankaccount");
        dialog.setHeaderText("How much money do you want to withdraw?");
        dialog.setContentText("Amount: ");
        Optional<String> result = dialog.showAndWait(); //liefert ein Optinal zurück
        if (result.isPresent()) { //wenn etwas zurück geliefert wurde
            try {
                BigDecimal amount = new BigDecimal(result.get());
                this.getBankAccount().withdraw(amount);
                this.lblBankaccountBalance.textProperty().setValue(this.getBankAccount().getBalance().toString());
                System.out.println("WITHDRAW");
            } catch (NumberFormatException numberFormatException) { //unchecked Exception
                WalletApp.showErrorDialog("Please insert a number!");
            } catch (InsufficientBalanceException insufficientBalanceException) {
                WalletApp.showErrorDialog(insufficientBalanceException.getMessage());
            }
        }
    }

    /**
     * Die Methode implementiert das Wechseln auf die Szene der wallet.fxml durch selektieren eines Tabelleneintrages.
     */
    //Abschnitt R
    public void openWallet() {
        //Abschnitt W
        Wallet wallet = this.tableView.getSelectionModel().getSelectedItem();
        if (wallet != null) {
            //speichern in HashMap um in der nächtsten Szene darauf zugreifen zu können
            GlobalContext.getGlobalContext().putStateFor(WalletApp.GLOBAL_SELECTED_WALLET, wallet);
            WalletApp.switchScene("wallet.fxml", "at.itkolleg.sample.wallet");
        } else {
            WalletApp.showErrorDialog("You have to select a Wallet first!");
        }
    }

    /**
     * Die Methode erlaubt das erzeugen einer neuen CryptoWallet über eine zuvor selektierte CryptoCurrency aus
     * der ComboBox.
     *
     * @throws InvalidFeeException
     */
    //Abschnitt R
    public void newWallet() throws InvalidFeeException {
        //Abschnitt T
        Object selectedItem = this.cmbWalletCurrency.getSelectionModel().getSelectedItem(); //Selektierter Wert
        if (selectedItem == null) {
            WalletApp.showErrorDialog("Choose currency!");
            return;
        }
        //selektirten Wert zuweisen
        CryptoCurrency selectedCryptoCurrency = CryptoCurrency.valueOf(this.cmbWalletCurrency.getSelectionModel().getSelectedItem().toString());
        this.getWalletList().addWallet(new Wallet(selectedCryptoCurrency.currencyName, selectedCryptoCurrency, new BigDecimal("1")));
        tableView.getItems().setAll(this.getWalletList().getWallesAsObservabletList());
    }


}
