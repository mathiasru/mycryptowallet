package ui;

import exceptions.GetCurrentPriceException;
import exceptions.InsufficientAmountException;
import exceptions.InsufficientBalanceException;
import exceptions.InvalidAmountException;
import at.itkolleg.sample.WalletApp;
import domain.CurrentPriceForCurrency;
import domain.Transaction;
import domain.Wallet;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Optional;

/**
 * Die Klasse implementiert die Logik und das Verhalten der Grafischen Oberfläche der Walletseite und erbt von der Klasse
 * BaseControllerState.
 * @author mathiasrudig
 * @version 1.0
 */
//Abschnitt V
public class WalletController extends BaseControllerState {
    //Abschnitt W
    @FXML
    private Button btnBackToMain;
    @FXML
    private Label lblId, lblName, lblCurrency, lblFee, lblAmount, lblValue;
    @FXML
    private TableView<Transaction> tblTransactions;

    private Wallet wallet;

    /**
     * Die Methode wird zum Initialisieren der Grafischen Oberfläche verwendet.
     */
    //Abschnitt W
    public void initialize() {
        //zuweisen der zuvor selektierten Wallet
        this.wallet = (Wallet) GlobalContext.getGlobalContext().getStateFor(WalletApp.GLOBAL_SELECTED_WALLET);
        this.populateTable();
        this.refreshAllGuiValues();

        //könnte auch als eigene Methode zb. back() implementiert werden
        btnBackToMain.setOnAction((ActionEvent e) -> { //Lamda Ausdruck (anonym)
            WalletApp.switchScene("main.fxml", "at.itkolleg.sample.main");
        });
    }

    /**
     * Hilfsmethode welche uns den aktuellen Kurs liefert.
     * @return Liefert den aktuellen Kurs vom Typ CurrentPriceForCurrency zurück.
     */
    //Abschnitt X
    private CurrentPriceForCurrency getCurrentPriceStrategie(){
        return (CurrentPriceForCurrency)GlobalContext.getGlobalContext().getStateFor(WalletApp.GLOBAL_CURRENT_CURRENCY_PRICES);
    }

    /**
     * Die Methode implementiert das Aktualisieren der Datenfelder, welche angezeigt werden und berechnet mithilfe der
     * getCurrentPriceStrategie()-Methode den aktuellen Wert des Wallet und weist diesen ebenfalls dem Datenfeld zu.
     */
    //Abschnitt X
    private void refreshAllGuiValues(){
        this.lblId.textProperty().setValue(this.wallet.getId().toString());
        this.lblName.textProperty().setValue(this.wallet.getName());
        this.lblCurrency.textProperty().setValue(this.wallet.getCryptoCurrency().getCode());
        this.lblFee.textProperty().setValue(this.wallet.getFeeInPercent().toString());
        this.lblAmount.textProperty().setValue(this.wallet.getAmount().toString());

        try {
            BigDecimal currentPrice = this.getCurrentPriceStrategie().getCurrentPrice(this.wallet.getCryptoCurrency());
            //System.out.println(currentPrice); //Debugging
            BigDecimal currentValue = currentPrice.multiply(this.wallet.getAmount()).setScale(6, RoundingMode.HALF_UP);
            this.lblValue.textProperty().setValue(currentValue.toString());
        } catch (GetCurrentPriceException getCurrentPriceException) {
            WalletApp.showErrorDialog(getCurrentPriceException.getMessage());
            this.lblValue.textProperty().setValue("CURRENT PREICE NOT AVAILABLE!");
        }

        //Einträge definieren aus der Transaktionsliste //Abschnitt Y
        tblTransactions.getItems().setAll(wallet.getTransactions());
    }

    /**
     * DIe Methode implementiert das Erzeugen einer Tabelle mit definierten Spalten und fügt Datensätze aus
     * Transaktionen hinzu.
     */
    //Abschnitt Y
    private void populateTable(){
        //Spalten der Tabelle definieren über Getter-Methoden
        TableColumn<Transaction, String> id = new TableColumn<>("ID");
        id.setCellValueFactory(new PropertyValueFactory<>("id"));

        TableColumn<Transaction, String> crypto = new TableColumn<>("CRYPTO");
        crypto.setCellValueFactory(new PropertyValueFactory<>("cryptoCurrency"));

        TableColumn<Transaction, String> amount = new TableColumn<>("AMOUNT");
        amount.setCellValueFactory(new PropertyValueFactory<>("amount"));

        TableColumn<Transaction, String> total = new TableColumn<>("TOTAL");
        total.setCellValueFactory(new PropertyValueFactory<>("total"));

        TableColumn<Transaction, String> priceAtTransactionDate = new TableColumn<>("PRICE");
        priceAtTransactionDate.setCellValueFactory(new PropertyValueFactory<>("priceAtTransactionDate"));

        TableColumn<Transaction, String> date = new TableColumn<>("DATE");
        date.setCellValueFactory(new PropertyValueFactory<>("date"));

        //Spalten hinzufügen
        tblTransactions.getColumns().clear();     //zu erst alles löschen
        tblTransactions.getColumns().add(id);
        tblTransactions.getColumns().add(crypto);
        tblTransactions.getColumns().add(amount);
        tblTransactions.getColumns().add(total);
        tblTransactions.getColumns().add(priceAtTransactionDate);
        tblTransactions.getColumns().add(date);
    }

    /**
     * Die Methode implementiert das Kaufen von crypto über ein erstelltes PopUp-dialogfenster und gibt im
     * Fehlerfall eine Nachricht in Form eines Alerts aus. Nach der Eingabe wird die Gui refreshed, damit alle
     * Werte aktuell sind.
     */
    //Abschnitt Z
    public void buy() {
        TextInputDialog dialog = new TextInputDialog("Amount of crypto to buy");
        dialog.setTitle("Buy crypto");
        dialog.setHeaderText("How much crypto do you want to buy?");
        dialog.setContentText("Amount: ");
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()){
            try{
                BigDecimal amount = new BigDecimal(result.get());
                this.wallet.buy(amount, this.getCurrentPriceStrategie().getCurrentPrice(wallet.getCryptoCurrency()), getBankAccount());
                this.refreshAllGuiValues();
                System.out.println("BUY CRYPTO");
            }catch (NumberFormatException numberFormatException){
                WalletApp.showErrorDialog("Invalid amount. Insert a number");
            } catch (GetCurrentPriceException | InvalidAmountException | InsufficientBalanceException exception) {
                WalletApp.showErrorDialog(exception.getMessage());
            }
        }
    }

    /**
     * Die Methode implementiert das Verkaufen von crypto über ein erstelltes PopUp-dialogfenster und gibt im
     * Fehlerfall eine Nachricht in Form eines Alerts aus. Nach der Eingabe wird die Gui refreshed, damit alle
     * Werte aktuell sind.
     */
    //Abschnitt Z
    public void sell() {
        TextInputDialog dialog = new TextInputDialog("Amount of crypto to sell");
        dialog.setTitle("Sell crypto");
        dialog.setHeaderText("How much crypto do you want to sell?");
        dialog.setContentText("Amount: ");
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()){
            try{
                BigDecimal amount = new BigDecimal(result.get());
                this.wallet.sell(amount, this.getCurrentPriceStrategie().getCurrentPrice(wallet.getCryptoCurrency()), getBankAccount());
                this.refreshAllGuiValues();
                System.out.println("SELL CRYPTO");
            }catch (NumberFormatException numberFormatException){
                WalletApp.showErrorDialog("Invalid amount. Insert a number");
            } catch (GetCurrentPriceException | InvalidAmountException | InsufficientAmountException exception) {
                WalletApp.showErrorDialog(exception.getMessage());
            }
        }
    }

}
