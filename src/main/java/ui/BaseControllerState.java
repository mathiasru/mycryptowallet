package ui;

import at.itkolleg.sample.WalletApp;
import domain.BankAccount;
import domain.WalletList;

/**
 * Die Klasse stellt gewisse Datenfelder und Methoden bereit, von der unsere restlichen Controller erben,
 * um Codeduplikationen zu vermeiden.
 *
 * @author mathiasrudig
 * @version 1.0
 */
//Abschnitt S
public class BaseControllerState {
    //Abschnitt S
    private WalletList walletList;
    private BankAccount bankAccount;

    /**
     * Der Konstruktor holt sich die Daten aus dem GlobalContext(HashMap) und weist sie dem jeweiligen Typ zu.
     */
    //Abschnitt S
    public BaseControllerState() {
        walletList = (WalletList) GlobalContext.getGlobalContext().getStateFor(WalletApp.GLOBAL_WALLET_LIST);
        bankAccount = (BankAccount) GlobalContext.getGlobalContext().getStateFor(WalletApp.GLOBAL_BANK_ACCOUNT);
    }

    /**
     * Die Methode liefert eine WalletList zurück.
     * @return Liefert eine WalletList vom Typ WalletList zurück.
     */
    //Abschnitt S
    public WalletList getWalletList() {
        return walletList;
    }

    /**
     * Die Methode liefert einen Bankaccount zurück.
     * @return Liefert einen Bankaccount vom Typ BankAccount zurück.
     */
    //Abschnitt S
    public BankAccount getBankAccount() {
        return bankAccount;
    }
}
