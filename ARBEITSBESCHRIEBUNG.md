# MyCryptoWalletApp

## LernInhalte MyCryptowalletApp:

Maven, JavaFX, in Dteien Speichern, exceptions, HttpClient, BigDecimal

## Abschnitt A - B:

Generieren und einrichten einer Maven Projektstruktur mit der Version Java JDK16.
[https://start.gluon.io](https://start.gluon.io/)

## Abschnitt C:

Implementieren der Klasse BankAccount unter Verwendung eines Marker Interface für späteres Schreiben in Files(Serializable). Das Abheben ist maximal bis auf Kontostand null erlaubt und wurde als Businesslogik implementiert. Als einziges Datefeld steht der Aktuelle Betrag(balance) vom Typ BigDecimal, welcher per sondierender-Methode von Außen abgerufen werden kann. Der Kontostand(balance) ist vom Typ  BigDecimal der eine genaue Berechnung auf Bestimmte Nachkommastellen gewähleistet.

### Verwendung von BigDecimal(sehr genau bei Kommastellen):

BigDecimal übergeben die Zahl als String dem Konstruktor.

Mit der add methode wird nicht direkt gerechnet, sondern musss in einer Variable zwischengespeichert werden.

```java
//Skala auf zwei Nachkommastellen aufrunden
this.balance = new BigDecimal("0").setScale(2, RoundingMode.HALF_UP);
//Vergleichen auf eine Zahl
if (fee.compareTo(new BigDecimal("0")) >= 0)
if (this.balance.subtract(amount).doubleValue() >= 0)
```

### Verwendung von exceptions:

Erstellen einer neuen Klasse mit dem Namen der Exception, welche von  Exception erbt.

Der Konstruktor kann mit einem String den Konstruktor der Mutterklasse(super("")) aufgerufen werden, welche über das Exceptionobjekt.getMessage() aufgerufen werden kann.

Um dem Nutzer der Methode über die Exception aufzuklären, muss in der Methodensignatur throws und der Klassenname der Exception geschrieben werden.

Wenn die Methoden dann aufgerufen wird, muss sie in ein try/catch gepackt werden um sie im Fehlerfall abfangen und einen Fehlermeldung schreiben zu können. mit finally könnte man noch etwas unabhängig davon machen oder nicht. Über exception.getMessage() kann der Text des Konstruktors auferufen werden.

## Abschnitt D:

Import einer bereitgestellten Enum-Klasse(mit Werten) CryptoCurrency und Implementieren der Klasse Transaction, welche eine Buchungszeile mit übergebenen und aktuellen Werten interpretiert. Diese sollte nach der Erzeugung nicht mehr veränderbar sein(alle Datenfelder final). In der genannten Klasse werden mehrere Bibliotheksklassen wie UUID, BigDecimal und LocalDate als final Datenfelder deklariert, damit die Buchungszeile vor Manipulation geschütz wird. Über sondierende Methoden wird der Zugriff auf die Werte von Außen gewährleistet.

## Abschnitt E:

Implementieren der Klasse Wallet, in der Kryptowährungen respektive Transaktionslisten gespeichert werden. In der Wallet können Kryptowährungen gerkauft beziehungsweise verkauft werden. Da sämtliche Datenfelder nicht mehr verändert werden sollen, wurde bei der sondierenden Methode, welche eine Liste von Transaktionen zurückliefert, nur eine Kopie über List.copyOf() zurückgegeben(Manipulation).  Zudem enthält sie Gebühren, welche per Businesslogik nur einen Wert größer gleich 0 halten dürfen. Diese  Buisnessregel zum Setzen von Fees wurde mittels Abfragen und einer Exception umgesetzt. Das Handling der Exception ist etwas speziell, da diese beim Erzeugen des Objektes über den Konstruktor geschmissen werden kann (Bubbling von exceptions).

## Abschnitt F:

Erweitern der Klasse Wallet um die Methode buy,  die das Kaufen von Kryptowährungen innerhalb einer erstellten Transaktion implementiert,
welche als Businessregel einen positiven Betrag größer 0 erwartet. Ansonsten wir eine Exception(InvalidAmountException) geschmissen. Zudem wird die Transaktion in die Transaktionsliste angefügt und der Betrag unter Berücksichtigung der Gebühren, dem BankAccount abgezogen.

## Abschnitt G:

Erweitern der Klasse Wallet um die Methode sell, die das Verkaufen von Kryptowährungen innerhalb einer erstellten Transaktion, welche als Businessregel einen positiven Betrag größer 0 erwartet. Ansonsten wir eine Exception(InvalidAmountException) oder bei zu hohem Betrag die Exception(InsufficientAmountException) geschmissen. Zudem wird die Transaktion in die Transaktionsliste angefügt und der Betrag unter Berücksichtigung der Gebühren, dem BankAccount gutgeschrieben.

## Abschnitt H:

Implementieren der Klasse WalletList, welche uns all unsere Wallets aufzeigt und wieder Serializable implementiert, um später ggf. in Dateien schreiben respektive aus Dateien lesen zu können. Zudem wurden Methoden implementiert, welche das Hinzufügen/ Abrufen von Wallets in einer HashMap ermöglichen. Die Methode getWalletsAsObservableList() erzeugt eine Liste der Wallets, welche sich die Values holt, genereirten aus diesen einenDatenstrom und erzeugt damit eine Liste, welche zurückgegeben wird.

```java
public List<Wallet> getWalletsAsObservableList(){
        return wallets.values().stream().collect(Collectors.toList());
    
```
## Abschnitt I:

Implementieren der Klasse CurrentCurrencyPrice mit der Methode getCurrentPrice, die einen HttpRequest enthält, welcher mittels Rest-API den aktuellen Kurs jeder jeweiligen Kryptowährung ausliest und richtig geparst wird. Die Klasse implementiert das Interface CurrentPriceForCurrency um flexibleren Code garantieren zu können.  Zudem werden mehrere Exception direkt innerhalb der Methode gehandelt (IOException, InterruptedException, NumberFormatException) und im Falle auch ausgegeben.  Ein HttpClient erzeugen für Get, Put Requests und wurde mit .newHttpClient() (statische Methode) initialisiert. Generieren eines HttpRequest, der auf eine implementierte URI angewendet wird und akzeptiert einen mitgegebenen Header. Der aktuelle Kurswert der Seite CoinGecko wird uns per Anfrage(HTTP-GET-Request) im JSON-Format bereitgestellt, welches noch richtig geparst werden muss, um nur den aktuellen Wert zu bekommen. Die neu erstellte Exception(GetCurrentPriceException) wurde mit übergabe Parameter implementiert, damit von Außen eine Message mitgegeben werden kann, um dynamische Zeichenketten augeben zu können.
```json
{
  "bitcoin": {
    "eur": 29665
  }
}
```

## Abschnitt J:

Implementieren des Interfaces DataStore, welches Methoden zum Speichern und Auslesen von Objekten der Klasse BankAccount und Walletlist bereitstellt.
Zusätzlich wurden für Beide Fälle exceptions erstellt (SaveDataException, RetrieveDataException) und die Klasse FileDataStore, welche das oben genannte Interface und deren Methoden implementiert. Innerhalb der Methoden werden die Objekte in Binärdateien geschrieben respektive ausgelesen. Wichtig dabei ist, dass die betroffenen Klassen das Marker-Interface Serializable implmentieren um das Speichern/Auslesen realisieren zu können.

```java
//Objekt anlegen zum Speichern
ObjectOutputStream objectOutputStream = null;
        try {
        objectOutputStream = new ObjectOutputStream(new FileOutputStream("account.bin"));
        }
```

```java
//Objekt anlegen zum Auslesen
ObjectInputStream objectInputStream = null;
        try {
            objectInputStream = new ObjectInputStream(new FileInputStream("walletlist.bin"));
}
```

## Abschnitt K:

Implementieren der  fehlenden Methoden der Klasse FileDataStore um Walletlists speichern und auslesen zu können.

## Abschnitt L:

Einführung in GUI mit JavaFX.

Die launch() Methode, startet den Einstiegspunkt der Grafischen Oberfläche(JavaFX), welche mittels FXMLLoader mit einem entsprechenden fxml-File einen AnchorPane erzeugt. Dieser kann als neue Szene angelegt, gesetzt und aufgerufen werden. Innerhalb der main.fxml Datei befindet sich die Konfiguration(css, Position, Verhalten(in MainController implementiert(@FXML) und über #Methodenname aus der main.fxml aufrufen) usw.) der FX-Anwendung. Zusätzlich kann die Konfiguration auch über ein Zusatztool(SceneBuilder) umgesetzt werden. Zudem werden im MainController auch Datenfelder angelegt, auf die mittels id und richtigem Typ zugegriffen werden kann(@FXML Kennzeichnung notwendig). Über die main.properties-Datei können die Texte(Key-Value-Paar) international gehalten werden, damit der Code nicht verändert werden muss.

## Abschnitt M:

Erstellen einer globalen Stage(mainStage) in der umbenannten Main-Klasse(WalletApp) und implementieren der statischen Methode switchScene(), die das wechseln der Szenen ermöglicht. Zudem wurde die start-Methode angepasst, um beim Hochfahren die übergebene Stage der mainStage zuzuweisen und die main.fxml aufrufen zu können. Weiters wurde eine Methode Alerts implementiert, welche von überall für Ausgaben von Fehlertexten(exceptions) verwedet werden.

## Abschnitt N:

Implementieren der Methoden zum Speichern und Laden der Wallet-List beziehungsweise des BankAccounts in der WalletApp-Klasse. Oben genannte Methoden in die start()-Methode implementiert um beim Starten des Programmes die letzte Sitzung wiederhestellen zu können.

## Abschnitt O:

Implementieren der Klasse GlobalContext um States zwischen den Szenen zu halten (Zwischenablage), welche nur von der Klasse(private Constructor) aus max. eine Instanz(Klassenvariable vom Typ der Klasse) bilden kann. Zudem stellt die Klasse veschiedenen öffentliche Methoden bereit, um Object ein ein HashMap(Name-Wert-Paar) zu schreiben respektive entfernen oder abzurufen. In der start()-Methode(live cycle method) werden die zuvor angelegten Objekte in die HashMap gespeichert, um später aus der neuen Stop()-Methode(live cycle method) geladen zu werden.

## Abschnitt P:

Bereinigen der Main-Methode und Überprüfen der Funktion inkl. Speichern der Files.

## Abschnitt Q:

Implementieren von GUI-Elemente anhand der main.fxml-Datei und Formatierung innerhalb der main.css-Datei. (VBOX = platziert die Elemente untereinander, AnchorPane/Pane = Zeichenfläche) Zudem wurden in der main.properties-Datei Texte der Labels defieniert und die Obberfläche getestet.

## Abschnitt R:
Erweiterung der Grafischen Oberfläche(verschiedene Buttons und Actions, Labels) und Anlegen der Methoden innerhalb des MainControllers. (Aufbau der Hauptseite)

## Abschnitt S:

Implementieren der Methoden innerhalb des MainControllers un setzen der Datenfelder(fx:id), welche mit @FXML gekennzeichnet werden müssen. Zudem wurde die Klasse BaseControllerState implementiert, von der unsere restlichen Controller gewisse Datenfelder und Methoden erben, um Codeduplikationen zu vermeiden. Weiters wurde die initialize()-Methode erweitert, um den aktuellen Wert des Bankaccounts anzeigen zu lassen. Die Methoden deposit() und withdraw() wurden mit einem Dialog/Eingabefenster und gewissen properties umgesetzt und behandeln die jeweiligen exceptions.

## Abschnitt T:

Erzeugen von einer Tabelle mit gewissen definierten Spalten(TableColumn), welche jeweils erzeugt und der Tabelle hinzugefügt(add()) werden müssen (innerhalb der inizialize()-Methode).  Zudem wurde die Methode newWallet() implementiert, welche es uns erlaubt aus einer zuvor selektierten CryptoCurrency ein neues Wallet zu erzeugen, oder ansonsten eine Fehlermeldung auszugeben.

```java
//Spalten der Tabelle definieren über Getter-Methoden
TableColumn<Wallet, String> symbol = new TableColumn<>("SYMBOL");
symbol.setCellValueFactory(new PropertyValueFactory<>("cryptoCurrency"));

//Spalten hinzufügen
tableView.getColumns().add(name);

//Einträge definieren aus der Walletlist
tableView.getItems().setAll(getWalletList().getWallesAsObservabletList());
```

## Abschnitt U:

Testen der Anwendung im Bezug auf das Erzeugen von Wallets und den jeweiligen Eintrag in die Tabelle.

## Abschnitt V:

Implementieren der Methode enterWallet() im MainController um auf die neue Szenen zu wechseln. Neue Grafikseite angelegt (wallet.css, wallet.fxml, wallet.properties) und die dazu gehörige Controller-Klasse(WalletController), welche die Datenfelder(fx:id) und Methoden implementiert. Zudem wurde die Grafische Oberfläche nach dem selben Schema wie bei der Hauptseite umgesetzt(Texte, Elemente, Methoden, Style). Die Verwendeten Methoden(buy, sell) wurden im WalletController nur erzeugt und werden im nächsten Schritt implementiert.

## Abschnitt W:

Implementieren der @FXML Datenfelder.
Umsetzen des zurück Buttons über die inizialize()-Methode mittels anonymer Funktion. Implementieren der openWallet()-Methode um mittels selektierten Eintrag einer Tabelle in die jeweilige Wallet zu wechseln(Szenenwechsel mittels statischer String Variable in WalletApp, um Eintrag in HashMap zwischenspeichern zu können).  Beim Initializieren der neuen Szenen wird auf den vorher erstellten Eintrag über den GlobalContext(HashMap) zugegriffen und das jeweilige Wallet aufgerufen.

## Abschnitt X:

Implementieren einer refresh-Methode, welche die Datenfelder, die angezeigt werden aktualisiert und über eine private Hilfsmethode den aktuellen Kurs mittels Amount berechnet und auf 6 Kommastellen genau gerundet wurde, zuweist.

## Abschnitt Y:

Implementieren der Methode populateTable(), welche uns die Tabelle mit definierten Spalten generiert und von der inizialize()-Methode aufgerufen wird. Zudem wurde der refreshGuiValues()-Methode noch das Einfügen der Transaktionen aus der Transaktionsliste(Kopie) hinzugefügt, um die erzeugten Einträge in der Tabele anzeigen zu lassen.

## Abschnitt Z:

Implementieren der Methoden buy() und sell() in der WalletController-Klasse mit einem erstellten Eingabe-Dialogfenster, um den Amount angeben zu können und nach erfolgreicher Eingabe wird die GUI-refreshed. Im Fehlerfall wird ein Alert angezeigt. Zudem wurde noch der exit-Button mittels anonymer Funktion implementiert und über folgenden Code wurde der schließen Button außer Betrieb gesetz:

```java
mainStage.setOnCloseRequest(event -> event.consume());
```

## Abschnitt Refactoring:

Auslagern gewisser Codeblöcke in eigenen Hilfsmethoden(MainController), Umbenennen des Package-Exceptions und Ausgaben gewisser Events auf der Kommandozeile implemnetiert.
